﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1_Project2
{
    internal class Loops
    {static void Main()
        {
            // do - while 
            // while
            // for
            // Every loop has 3 statements
            // 1. STarting point
            // 2. Condition/Termination point
            // 3. Inc / Dec statement

            // print frm 1 to 10
            int n = 11;
            do
            {
                Console.WriteLine(n);
                n++;
            } while (n <= 10);


            // while loop

            n = 11;
            while (n <= 10) 
             {
                Console.WriteLine(n);
                n++;
            }
            
            for(int i = 11; i<=10; i++)
            {  Console.WriteLine(i); }
        }
    }
}
