﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1_Project2
{
    internal class Program2
    {
        static void Main()
        {
            int c =  Console.Read();
            Console.WriteLine((char)c);
            Console.WriteLine("Enter Name");
            string name = Console.ReadLine(); 
            Console.WriteLine("Enter RollNO");
            int rn = Byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter Batch Code");
            string batchCode = Console.ReadLine();
            Console.WriteLine("Enter Marks");
            int marks = Convert.ToByte(Console.ReadLine());
            Console.WriteLine($"Name is {name}\nRollNo is {rn}\n" +

                $"BatchCode is {batchCode}\nMarks are {marks}");
            //Int16.Parse
            //Convert.ToInt32(Console.ReadLine());
            //Byte.Parse(Console.ReadLine());
            //Convert.ToByte(Console.ReadLine());

        }
    }
}